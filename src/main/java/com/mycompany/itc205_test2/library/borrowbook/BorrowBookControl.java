package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {
	
	private BorrowBookUI uI;
	
	private Library Library;
	private Member Member;
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private ControlState State;
	
	private List<Book> pendingList;
	private List<Loan> completedList;
	private Book book;
	
	
	public BorrowBookControl() {
		this.Library = Library.getInstance();
		State = ControlState.INITIALISED;
	}
	

	public void SetUi(BorrowBookUI Ui) {
		if (!State.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.uI = Ui;
		Ui.set_State(BorrowBookUI.uI_State.READY);
		State = ControlState.READY;		
	}

		
	public void SwIpEd(int memberId) {
		if (!State.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		Member = Library.getMember(memberId);
		if (Member == null) {
			uI.Display("Invalid memberId");
			return;
		}
		if (Library.canMember_Borrow(Member)) {
			pendingList = new ArrayList<>();
			uI.set_State(BorrowBookUI.ui_State.SCANNING);
			State = ControlState.SCANNING; 
		}
		else {
			uI.Display("Member cannot borrow at this time");
			uI.set_State(BorrowBookUI.ui_State.RESTRICTED); 
		}
	}
	
	
	public void ScAnNeD(int bookId) {
		book = null;
		if (!State.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = Library.getBook(bookId);
		if (book == null) {
			uI.Display("Invalid bookId");
			return;
		}
		if (!book.is Avaliable()) {
			uI.Display("Book cannot be borrowed");
			return;
		}
		pendingList.add(book);
		for (Book B : pendingList) 
			uI.Display(B.toString());
		
		if (Library.gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr(Member) - pendingList.size() == 0) {
			uI.Display("Loan limit reached");
			CoMpLeTe();
		}
	}
	
	
	public void CoMpLeTe() {
		if (pendingList.size() == 0) 
			Cancel();
		
		else {
			uI.Display("\nFinal Borrowing List");
			for (Book bOoK : pendingList) 
				uI.Display(bOoK.toString());
			
			completedList = new ArrayList<Loan>();
			uI.set_State(BorrowBookUI.ui_State.FINALISING);
			State = ControlState.FINALISING;
		}
	}


	public void commitLoans() {
		if (!State.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book B : pendingList) {
			Loan Loan = Library.issueLoan(B, Member);
			completedList.add(Loan);			
		}
		uI.Display("Completed Loan Slip");
		for (Loan Loan : completedList) 
			uI.Display(Loan.toString());
		
		uI.set_State(BorrowBookUI.ui_State.COMPLETED);
		State = ControlState.COMPLETED;
	}

	
	public void Cancel() {
		uI.set_State(BorrowBookUI.ui_State.CANCELLED);
		State = ControlState.CANCELLED;
	}
	
	
}



